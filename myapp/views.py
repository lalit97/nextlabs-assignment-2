from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from django.shortcuts import redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import CreateView, ListView
from django.views.generic.base import TemplateView

from .forms import TaskForm
from .models import Application, Task
from .utils import get_user_points


@method_decorator(staff_member_required(login_url='accounts:admin_login'), name='dispatch')
class ApplicationCreateView(CreateView):
    model = Application
    fields = '__all__'
    success_url = reverse_lazy('myapp:admin_home')
    template_name = 'myapp/application_create.html'


@method_decorator(staff_member_required(login_url='accounts:admin_login'), name='dispatch')
class AdminHomeView(ListView):
    model = Application
    context_object_name = 'apps'
    template_name = 'myapp/admin_home.html'


@method_decorator(login_required, name='dispatch')
class UserHomeView(ListView):
    context_object_name = 'apps'
    template_name = 'myapp/user_home.html'

    def get_queryset(self):
        claimed_apps_id = list(
            Task.objects.filter(user=self.request.user).
            values_list('application_id', flat=True)
        )
        not_claimed_apps = Application.objects.exclude(id__in=claimed_apps_id)
        return not_claimed_apps


@method_decorator(login_required, name='dispatch')
class UserProfileView(TemplateView):
    template_name = "myapp/user_profile.html"


@method_decorator(login_required, name='dispatch')
class UserPointsView(View):
    def get(self, request, *args, **kwargs):
        points = get_user_points(request.user)
        context = {
            'points': points
        }
        return render(request, "myapp/user_points.html", context)


@method_decorator(login_required, name='dispatch')
class TaskCreateView(View):
    form_class = TaskForm
    template_name = 'myapp/task_create.html'

    def get_context_data(self, **kwargs):
        app_id = self.kwargs['app_id']
        app = Application.objects.get(id=app_id)
        context = {
            'app': app,
            'form': self.form_class
        }
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return render(request, self.template_name, context)
    
    def post(self, request, *args, **kwargs):
        user = self.request.user
        context = self.get_context_data(**kwargs)
        app = context['app']
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            img = form.cleaned_data['image']
            fs = FileSystemStorage()
            image = fs.save(img.name, img)
            task = Task.objects.create(user=user, application=app, image=image)
            return redirect(reverse('myapp:user_home'))
        return render(request, self.template_name, context)
    

@method_decorator(login_required, name='dispatch')
class TaskClaimedView(ListView):
    context_object_name = 'tasks'
    template_name = 'myapp/tasks_claimed.html'

    def get_queryset(self):
        user = self.request.user
        queryset = Task.objects.filter(user=user)
        return queryset
