from django.db import models
from django.contrib.auth.models import User


class Application(models.Model):
    PROGRAMMING = 'PR'
    SPORTS = 'SP'
    HEALTH = 'HE'
    TRAVEL = 'TR'
    ENTERTAINMENT = 'EN'
    CATEGORY_CHOICES = [
        (PROGRAMMING, 'Programming'),
        (SPORTS, 'Sports'),
        (HEALTH, 'Health'),
        (TRAVEL, 'Travel'),
        (ENTERTAINMENT, 'Entertainment'),
    ]
    name = models.CharField(max_length=40)
    url = models.URLField()
    category = models.CharField(
        max_length=2,
        choices=CATEGORY_CHOICES,
        default=PROGRAMMING,
    )
    points = models.PositiveSmallIntegerField()

    def __str__(self):
        return f'{self.name}'


class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='tasks')

    def __str__(self):
        return f'{self.user.username}-{self.application.name}'
