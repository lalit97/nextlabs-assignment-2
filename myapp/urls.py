from . import views
from django.urls import path
from django.views.generic import TemplateView


app_name = 'myapp'
urlpatterns = [
    path('', TemplateView.as_view(template_name='myapp/home.html'), name='home'),
    path('user-home/', views.UserHomeView.as_view(), name='user_home'),
    path('admin-home/', views.AdminHomeView.as_view(), name='admin_home'),
    path('user-profile/', views.UserProfileView.as_view(), name='user_profile'),
    path('user-points/', views.UserPointsView.as_view(), name='user_points'),
    path('task-create/<int:app_id>/', views.TaskCreateView.as_view(), name='task_create'),
    path('tasks-claimed/', views.TaskClaimedView.as_view(), name='task_claimed'),
    path('application-create/', 
        views.ApplicationCreateView.as_view(), 
        name='application_create'
    ),
]
