from .models import Task

def get_user_points(user):
    tasks = Task.objects.filter(user=user)
    points = 0
    for task in tasks:
        points += task.application.points
    return points
