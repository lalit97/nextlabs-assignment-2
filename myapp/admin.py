from django.contrib import admin
from .models import Application, Task

admin.site.register(Application)
admin.site.register(Task)
