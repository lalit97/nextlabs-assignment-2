# Nextlabs Assignment 2

Live at http://nextlabs-assign-2.herokuapp.com/

## Sample Video

[Sample Video](https://gitlab.com/lalit97/nextlabs-assignment-2/-/blob/master/Sample.mov)

## Local Setup

- Git clone: `https://gitlab.com/lalit97/nextlabs-assignment-2.git`
- Go to the project directory: `cd nextlabs-assignment-2`
- Setup virtual environment: `virtualenv -p python3 .` (do not forget to add the dot(.) in the end)
- Activate virtual environment: `source bin/activate`
- Generate `SECRET_KEY` using [src/generate_secret.py](https://gitlab.com/lalit97/nextlabs-assignment-2/-/blob/master/generate_secret.py) and create an environment variable named as `SECRET_KEY` using it.
- Install requirements: `pip install -r requirements.txt`
- Run migrations: `python manage.py makemigrations`, `python manage.py migrate`
- Run server using local settings: `python manage.py runserver`
- See it running on [localhost](http://127.0.0.1:8000/)
