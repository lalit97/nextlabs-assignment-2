from django import forms
from django.contrib.auth.models import User 


class UserRegisterForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)   
    class Meta:
        model = User
        fields = ('username', 'first_name','last_name', 'email', 'password',)


class UserLoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(widget=forms.PasswordInput)   

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username') 
        password = self.cleaned_data.get('password')

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise forms.ValidationError("User does not exist")
        if not user.check_password(password):
            raise forms.ValidationError("Incorrect Password")
        return super(UserLoginForm, self).clean(*args, **kwargs)


class AdminLoginForm(UserLoginForm):
    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username') 
        password = self.cleaned_data.get('password')

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise forms.ValidationError("User does not exist")
        if not user.check_password(password):
            raise forms.ValidationError("Incorrect Password")
        if not user.is_staff:
            raise forms.ValidationError("User is not Admin")
        return super(AdminLoginForm, self).clean(*args, **kwargs)
